# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see https://www.gnu.org/licenses/.

nginx_domains:
  - name: '{{ ansible_host }}'
    config: |
      ssl_client_certificate /opt/nginx/ssl/internal/ca.crt;
      ssl_verify_client on;
      ssl_verify_depth 2;

      {% for exporter in prometheus_exporters %}

      location /metrics/{{ exporter }} {
          allow {{ hostvars.aeryn.ansible_host }};
          deny all;

          proxy_set_header Host $host;
          proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

          proxy_pass http://127.0.0.1:{{ prometheus_packages[exporter].port }}/metrics;
      }
      {% endfor %}

  - name: goupile.fr
    ssl_certbot_email: niels.martignene@protonmail.com
    config: |
      location / {
          return 301 https://goupile.org$request_uri;
      }
  - name: www.goupile.fr
    ssl_certbot_email: niels.martignene@protonmail.com
    config: |
      location / {
          return 301 https://goupile.org$request_uri;
      }

  - name: goupile.org
    ssl_certbot_email: niels.martignene@protonmail.com
    config: |
      location / {
          index index.html;

          rewrite ^((?:/.+)*/[a-zA-Z0-9]+)$ $1.html;
          rewrite ^((?:/.+)*/[a-zA-Z0-9]+)/$ $1 permanent;
          try_files $uri $uri/ =404;

          root /opt/static/live/goupile.org/www;

          expires 1h;
          add_header Cache-Control "public, no-transform";

          gzip on;
          gzip_vary on;
          gzip_types text/html text/plain text/css text/xml text/javascript application/javascript application/xml image/svg+xml;
          gzip_proxied no-cache no-store private expired auth;
          gzip_min_length 1000;
          gzip_static on;

          location /static/ {
              expires 28d;
          }
      }

      location /files/ {
          alias /opt/static/live/goupile.org/files/;

          location ~ ^/files/[a-zA-Z0-9_]+/ {
              autoindex on;
              autoindex_exact_size off;
          }
      }
  - name: www.goupile.org
    ssl_certbot_email: niels.martignene@protonmail.com
    config: |
      location / {
          return 301 https://goupile.org$request_uri;
      }

  - name: koromix.dev
    ssl_certbot_email: niels.martignene@protonmail.com
    config: |
      location / {
          index index.html;

          rewrite ^((?:/.+)*/[a-zA-Z0-9]+)$ $1.html;
          rewrite ^((?:/.+)*/[a-zA-Z0-9]+)/$ $1 permanent;
          try_files $uri $uri/ =404;

          root /opt/static/live/koromix.dev/www;

          expires 1h;
          add_header Cache-Control "public, no-transform";

          gzip on;
          gzip_vary on;
          gzip_types text/html text/plain text/css text/xml text/javascript application/javascript application/xml image/svg+xml;
          gzip_proxied no-cache no-store private expired auth;
          gzip_min_length 1000;
          gzip_static on;

          location /static/ {
              expires 28d;
          }
      }

      location /test/ {
          add_header Cross-Origin-Opener-Policy "same-origin" always;
          add_header Cross-Origin-Embedder-Policy "require-corp" always;

          alias /opt/static/live/koromix.dev/test/;
      }

      # Some stuff lives at home

      location /files/ {
          proxy_ssl_trusted_certificate /opt/nginx/ssl/internal/ca.crt;
          proxy_ssl_certificate /opt/nginx/ssl/internal/{{ inventory_hostname }}.crt;
          proxy_ssl_certificate_key /opt/nginx/ssl/internal/{{ inventory_hostname }}.key;
          proxy_pass https://192.168.192.250;
      }
      location /R/ {
          proxy_ssl_trusted_certificate /opt/nginx/ssl/internal/ca.crt;
          proxy_ssl_certificate /opt/nginx/ssl/internal/{{ inventory_hostname }}.crt;
          proxy_ssl_certificate_key /opt/nginx/ssl/internal/{{ inventory_hostname }}.key;
          proxy_pass https://192.168.192.250;
      }
      location /procemot/ {
          client_max_body_size 256M;
          proxy_ssl_trusted_certificate /opt/nginx/ssl/internal/ca.crt;
          proxy_ssl_certificate /opt/nginx/ssl/internal/{{ inventory_hostname }}.crt;
          proxy_ssl_certificate_key /opt/nginx/ssl/internal/{{ inventory_hostname }}.key;
          proxy_pass https://192.168.192.250;
      }
  - name: www.koromix.dev
    ssl_certbot_email: niels.martignene@protonmail.com
    config: |
      location / {
          return 301 https://koromix.dev$request_uri;
      }
  - name: download.koromix.dev
    ssl_certbot_email: niels.martignene@protonmail.com
    config: |
      location / {
          proxy_ssl_trusted_certificate /opt/nginx/ssl/internal/ca.crt;
          proxy_ssl_certificate /opt/nginx/ssl/internal/{{ inventory_hostname }}.crt;
          proxy_ssl_certificate_key /opt/nginx/ssl/internal/{{ inventory_hostname }}.key;
          proxy_pass https://192.168.192.250/download/;
      }
  - name: photo.koromix.dev
    ssl_certbot_email: niels.martignene@protonmail.com
    config: |
      location / {
          proxy_set_header Upgrade $http_upgrade;
          proxy_set_header Connection $connection_upgrade;

          proxy_ssl_trusted_certificate /opt/nginx/ssl/internal/ca.crt;
          proxy_ssl_certificate /opt/nginx/ssl/internal/{{ inventory_hostname }}.crt;
          proxy_ssl_certificate_key /opt/nginx/ssl/internal/{{ inventory_hostname }}.key;
          proxy_pass https://192.168.192.250/photo/;
      }
  - name: thop.koromix.dev
    ssl_certbot_email: niels.martignene@protonmail.com
    config: |
      location / {
          proxy_http_version 1.1;
          proxy_buffering on;
          proxy_read_timeout 180;
          send_timeout 180;

          proxy_set_header Host $host;
          proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

          proxy_pass http://unix:/run/thop/thop.koromix.dev.sock:;
      }
  - name: games.koromix.dev
    ssl_certbot_email: niels.martignene@protonmail.com
    config: |
      location / {
          index index.html;

          root /opt/static/live/koromix.dev/games;

          expires 1h;
          add_header Cache-Control "public, no-transform";
          add_header Cross-Origin-Opener-Policy "same-origin" always;
          add_header Cross-Origin-Embedder-Policy "require-corp" always;

          location ~ /static/ {
              expires 28d;
          }
      }
  - name: stat.koromix.dev
    ssl_certbot_email: niels.martignene@protonmail.com
    config: |
      location / {
          root /opt/goaccess/html/koromix.dev/;

          auth_basic "stat.koromix.dev";
          auth_basic_user_file /opt/goaccess/html/koromix.dev.htpasswd;
      }

  - name: watch.koromix.dev
    ssl_certbot_email: niels.martignene@protonmail.com
    config: |
      location / {
          # limit_req zone=default burst=100 nodelay;

          proxy_set_header Host $host;
          proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

          proxy_pass http://127.0.0.1:9200;
      }

  - name: koffi.dev
    ssl_certbot_email: niels.martignene@protonmail.com
    config: |
      location / {
          index index.html;

          rewrite ^((?:/.+)*/[a-zA-Z0-9]+)$ $1.html;
          rewrite ^((?:/.+)*/[a-zA-Z0-9]+)/$ $1 permanent;
          try_files $uri $uri/ =404;

          root /opt/static/live/koffi.dev/www;

          expires 1h;
          add_header Cache-Control "public, no-transform";

          gzip on;
          gzip_vary on;
          gzip_types text/html text/plain text/css text/xml text/javascript application/javascript application/xml image/svg+xml;
          gzip_proxied no-cache no-store private expired auth;
          gzip_min_length 1000;
          gzip_static on;

          location /static/ {
              expires 28d;
          }
      }
  - name: stat.koffi.dev
    ssl_certbot_email: niels.martignene@protonmail.com
    config: |
      location / {
          root /opt/goaccess/html/koffi.dev/;

          auth_basic "stat.koffi.dev";
          auth_basic_user_file /opt/goaccess/html/koffi.dev.htpasswd;
      }

  - name: demheter.fr
    ssl_certbot_email: niels.martignene@protonmail.com
    config: |
      location /admin/ {
          index index.php index.html index.htm;

          root /opt/static/live/demheter.fr/www;

          location ~ \.php$ {
              include fastcgi_params;
              fastcgi_param SCRIPT_FILENAME $request_filename;
              fastcgi_pass unix:/run/php/php8.2-fpm.sock;
          }
      }
      location = /admin {
          return 301 https://$host/admin/;
      }

      location / {
          index index.php index.html index.htm;

          rewrite ^/index$ /index.php;
          rewrite ^((?:/.+)*/[a-zA-Z0-9]+)$ $1.html;
          rewrite ^((?:/.+)*/[a-zA-Z0-9]+)/$ $1 permanent;
          try_files $uri $uri/ =404;

          root /opt/static/live/demheter.fr/www;

          expires 1h;
          add_header Cache-Control "public, no-transform";

          gzip on;
          gzip_vary on;
          gzip_types text/html text/plain text/css text/xml text/javascript application/javascript application/xml image/svg+xml;
          gzip_proxied no-cache no-store private expired auth;
          gzip_min_length 1000;
          gzip_static on;

          location /static/ {
              expires 28d;
          }

          location = /courier {
            return 301 https://$host/courrier;
          }
          location = /usagers {
            return 301 https://$host/apprendre;
          }
          location = /professionnels {
            return 301 https://$host/ressources;
          }
          location = /apprendre {
            return 301 https://$host/comprendre;
          }

          location ~ \.php$ {
              include fastcgi_params;
              fastcgi_param SCRIPT_FILENAME $request_filename;
              fastcgi_pass unix:/run/php/php8.2-fpm.sock;
          }
      }
  - name: beta.demheter.fr
    ssl_certbot_email: niels.martignene@protonmail.com
    config: |
      location /admin/ {
          index index.php index.html index.htm;

          root /opt/static/live/demheter.fr/beta;

          location ~ \.php$ {
              include fastcgi_params;
              fastcgi_param SCRIPT_FILENAME $request_filename;
              fastcgi_pass unix:/run/php/php8.2-fpm.sock;
          }
      }
      location = /admin {
          return 301 https://$host/admin/;
      }

      location / {
          index index.php index.html index.htm;

          rewrite ^/index$ /index.php;
          rewrite ^((?:/.+)*/[a-zA-Z0-9]+)$ $1.html;
          rewrite ^((?:/.+)*/[a-zA-Z0-9]+)/$ $1 permanent;
          try_files $uri $uri/ =404;

          root /opt/static/live/demheter.fr/beta;

          expires 1h;
          add_header Cache-Control "public, no-transform";

          gzip on;
          gzip_vary on;
          gzip_types text/html text/plain text/css text/xml text/javascript application/javascript application/xml image/svg+xml;
          gzip_proxied no-cache no-store private expired auth;
          gzip_min_length 1000;
          gzip_static on;

          location /static/ {
              expires 28d;
          }

          location ~ \.php$ {
              include fastcgi_params;
              fastcgi_param SCRIPT_FILENAME $request_filename;
              fastcgi_pass unix:/run/php/php8.2-fpm.sock;
          }
      }
  - name: stat.demheter.fr
    ssl_certbot_email: niels.martignene@protonmail.com
    config: |
      location / {
          root /opt/goaccess/html/demheter.fr/;

          auth_basic "stat.demheter.fr";
          auth_basic_user_file /opt/goaccess/html/demheter.fr.htpasswd;
      }

  - name: carto.cn2r.fr
    ssl_certbot_email: niels.martignene@protonmail.com
    config: |
      location / {
          proxy_pass http://127.0.0.1:9977/cn2r/;
      }

      # Some stuff still lives on the old server
      location /R/ {
          proxy_pass http://192.168.192.250;
      }

  - name: carto.demheter.fr
    ssl_certbot_email: niels.martignene@protonmail.com
    config: |
      location / {
          proxy_pass http://127.0.0.1:9977/demheter/;
      }

  - name: carto.koromix.dev
    ssl_certbot_email: niels.martignene@protonmail.com
    config: |
      location / {
          proxy_pass http://127.0.0.1:9977/;
      }

  - name: test.cn2r.fr
    ssl_certbot_email: niels.martignene@protonmail.com
    config: |
      location / {
          alias /opt/static/live/cn2r.fr/test/;

          location /track/ {
              add_header Cross-Origin-Opener-Policy "same-origin" always;
              add_header Cross-Origin-Embedder-Policy "require-corp" always;
          }
      }

  - name: rekkord.org
    ssl_certbot_email: niels.martignene@protonmail.com
    config: |
      location / {
          index index.html;

          rewrite ^((?:/.+)*/[a-zA-Z0-9]+)$ $1.html;
          rewrite ^((?:/.+)*/[a-zA-Z0-9]+)/$ $1 permanent;
          try_files $uri $uri/ =404;

          root /opt/static/live/rekkord.org/www;

          expires 1h;
          add_header Cache-Control "public, no-transform";

          gzip on;
          gzip_vary on;
          gzip_types text/html text/plain text/css text/xml text/javascript application/javascript application/xml image/svg+xml;
          gzip_proxied no-cache no-store private expired auth;
          gzip_min_length 1000;
          gzip_static on;

          location /static/ {
              expires 28d;
          }
      }
  - name: www.rekkord.org
    ssl_certbot_email: niels.martignene@protonmail.com
    config: |
      location / {
          return 301 https://rekkord.org$request_uri;
      }

  - name: ldv-recherche.fr
    ssl_certbot_email: niels.martignene@protonmail.com
    config: |
      location / {
          index index.html;

          root /opt/static/live/ldv-recherche.fr/wip;

          expires 1h;
          add_header Cache-Control "public, no-transform";
      }
  - name: beta.ldv-recherche.fr
    ssl_certbot_email: niels.martignene@protonmail.com
    config: |
      location / {
          index index.html;

          rewrite ^((?:/.+)*/[a-zA-Z0-9]+)$ $1.html;
          rewrite ^((?:/.+)*/[a-zA-Z0-9]+)/$ $1 permanent;
          try_files $uri $uri/ =404;

          root /opt/static/live/ldv-recherche.fr/www;

          expires 1h;
          add_header Cache-Control "public, no-transform";

          add_header Cross-Origin-Opener-Policy "same-origin" always;
          add_header Cross-Origin-Embedder-Policy "require-corp" always;

          gzip on;
          gzip_vary on;
          gzip_types text/html text/plain text/css text/xml text/javascript application/javascript application/xml image/svg+xml;
          gzip_proxied no-cache no-store private expired auth;
          gzip_min_length 1000;
          gzip_static on;

          location /static/ {
              expires 28d;
          }
      }

  - name: git.interactions-team.fr
    ssl_certbot_email: niels.martignene@protonmail.com
    config: |
      location / {
          proxy_http_version 1.1;
          proxy_buffering on;
          proxy_read_timeout 180;
          send_timeout 180;

          proxy_request_buffering off;
          client_max_body_size 1G;

          proxy_set_header Host $host;
          proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

          proxy_pass http://127.0.0.1:3000/;
      }

  - name: team-interactions.fr
    ssl_certbot_email: niels.martignene@protonmail.com
    config: |
      location / {
          return 301 https://interactions-team.fr$request_uri;
      }
  - name: cloud.team-interactions.fr
    ssl_certbot_email: niels.martignene@protonmail.com
    config: |
      location / {
          return 301 https://cloud.interactions-team.fr$request_uri;
      }
  - name: drop.team-interactions.fr
    ssl_certbot_email: niels.martignene@protonmail.com
    config: |
      location / {
          return 301 https://drop.interactions-team.fr$request_uri;
      }
  - name: git.team-interactions.fr
    ssl_certbot_email: niels.martignene@protonmail.com
    config: |
      location / {
          return 301 https://git.interactions-team.fr$request_uri;
      }
  - name: vault.team-interactions.fr
    ssl_certbot_email: niels.martignene@protonmail.com
    config: |
      location / {
          return 301 https://vault.interactions-team.fr$request_uri;
      }

goaccess_domains:
  - name: demheter.fr
    username: stat
    password: !vault |
      $ANSIBLE_VAULT;1.1;AES256
      31646339393539396631636263646534323735646334383236353133363738366362646435323164
      3336303837303030663932653236336261633437636330620a626631616235663965653435633663
      36623036393432383731303261363831383861633339653635623435363464333134363232343162
      6133396536623661360a663236663565373130303536383030396466653635313635643664666661
      3361
  - name: koffi.dev
    username: stat
    password: !vault |
      $ANSIBLE_VAULT;1.1;AES256
      62393436306364323162386131313834653434626364643036323433373834396538363534333636
      6162613164306637653038333933326566663237643661390a336333633536643566323966373531
      35366566613939306435363435326634346562356638626137316262666562363466393033306136
      3664393439373861330a666665373233393931333064323563643665316236633766656362363330
      3234
  - name: koromix.dev
    username: stat
    password: !vault |
      $ANSIBLE_VAULT;1.1;AES256
      33333663633964613835616338613032323332393731393439346166653835626366363066373032
      3766656436336533623831623163336532343931383966390a393466333334396361313438633137
      31356539646164333835383139613131396366643631326539373263643738396238383435363466
      6433393437383033370a633063383466346639666532366531306536626165303063656366386535
      64643136343263373534363363643237313434636333653033623734653937393339

prometheus_exporters:
  - node_exporter
  - nginx_exporter
  - systemd_exporter

grafana_url: 'https://watch.koromix.dev/'
grafana_default_password: !vault |
  $ANSIBLE_VAULT;1.1;AES256
  66373865343436663235383632393034616561663934653035353238643037626333346635373131
  6562373135626532333664313635326633653538353737300a323333633733313162336161336366
  33336133376434333264653838663862393331633836633161376330653534633664633461346165
  6163623738623139300a623232363634363637663037343833623730616430323037396139356161
  33336330313061643466356361343732393664303337613033353430363762356265393761643163
  6636383333313135313866326262663734646338333234316662

grafana_smtp:
  host: mail.smtp2go.com:587
  username: !vault |
    $ANSIBLE_VAULT;1.1;AES256
    34626534646131383635393963346536373830663139626361353736376532353932326236333836
    3036353661663036613538383837646633643864326365650a653230393635643262633237383339
    36663763303533643838313562356239646231623737656239376264356535313439373533633533
    3633316539386635380a373832616365623134356434333662663361373233343662323131353066
    3262
  password: !vault |
    $ANSIBLE_VAULT;1.1;AES256
    38363339353632663035386138306330653637666235386363633962313735323565373633383065
    3961386139386235616337626130356433663964303137640a636639306563373133653939346330
    64376639396230383862663566346137386637376336663634613561613938356434323463316462
    3036313163636534640a636332323030346536343361366461303734633265666162343033396234
    35646430303463353032383534346638333062666136396537353136653864633632
  from: no-reply@koromix.dev
  start_tls: MandatoryStartTLS

grafana_dashboards: '{{ inventory_dir }}/../../config/prometheus/dashboards'
grafana_alerts: '{{ inventory_dir }}/../../config/prometheus/alerting'
