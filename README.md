# Inffra

Service de déploiement d'outils libres et sécurisés, modulables, à destination d'associations.

# Déploiements existants

## MLA

Suivez la [documentation MLA](mla/) pour plus d'informations.

## PKnet

Suivez la [documentation PKnet](pknet/) pour plus d'informations.
