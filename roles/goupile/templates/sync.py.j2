#!/bin/env python3

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see https://www.gnu.org/licenses/.

import argparse
import configparser
import hashlib
import io
import itertools
import json
import os
import re
import shutil
import sys
import subprocess
from dataclasses import dataclass

@dataclass
class DomainConfig:
    demo_mode = False
    main_directory = None
    archive_directory = None
    snapshot_directory = None
    socket = None
    archive_key = None
    mismatch = False

@dataclass
class ServiceStatus:
    running = False
    inode = None

def parse_ini(filename, allow_no_value = False):
    ini = configparser.ConfigParser(allow_no_value = allow_no_value)
    ini.optionxform = str

    with open(filename, 'r') as f:
        ini.read_file(f)

    return ini

def load_config(filename):
    ini = parse_ini(filename, allow_no_value = True)
    config = {}

    for section in ini.sections():
        for key, value in ini.items(section):
            if not section in config:
                config[section] = {}
            config[section][key] = value

            name = f'{section}.{key}'
            config[name] = value

    return config

def execute_command(args):
    subprocess.run(args, check = True)

def commit_file(filename, data):
    try:
        with open(filename, 'rb') as f:
            m = hashlib.sha256()
            for chunk in iter(lambda: f.read(4096), b""):
                m.update(chunk)
            hash1 = m.digest()
    except Exception:
        hash1 = None

    data = data.encode('UTF-8')
    m = hashlib.sha256()
    m.update(data)
    hash2 = m.digest()

    if hash1 != hash2:
        with open(filename, 'wb') as f:
            f.write(data)
        return True
    else:
        return False

def list_domains(key_dir, root_dir, archive_dir, snapshot_dir):
    domains = {}

    for name in os.listdir(key_dir):
        if not name.endswith('.ini'):
            continue

        filename = os.path.join(key_dir, name)
        name, _ = os.path.splitext(name)

        config = load_config(filename)
        section = config['Domain']

        domain = DomainConfig()

        domain.name = name
        domain.main_directory = os.path.join(root_dir, name)
        domain.archive_directory = os.path.join(archive_dir, name)
        domain.snapshot_directory = os.path.join(snapshot_dir, name)
        domain.socket = f'/run/goupile/{name}.sock'
        domain.archive_key = section['ArchiveKey']
        if 'DemoMode' in section:
            domain.demo_mode = decode_bool(section['DemoMode'])

        if domain.name in domains:
            raise ValueError(f'Name "{domain.name}" used multiple times')

        domains[domain.name] = domain

    return domains

def decode_bool(s):
    s = s.lower()

    if s in ('y', 'yes', 't', 'true', 'on', '1'):
        return True
    elif s in ('n', 'no', 'f', 'false', 'off', '0'):
        return False
    else:
        raise ValueError(f'Invalid truth value "{s}"')

def create_domain(binary, domain, main_dir, archive_key,
                  owner_user, owner_group, admin_username, admin_password):
    print(f'  + Create domain {domain} ({main_dir})', file = sys.stderr)

    execute_command([binary, 'init', '-o', owner_user, '--archive_key', archive_key,
                     '--title', domain, '--username', admin_username,
                     '--password', admin_password, main_dir])

def list_services():
    services = {}

    output = subprocess.check_output(['systemctl', 'list-units', '--type=service', '--all'])
    output = output.decode()

    for line in output.splitlines():
        parts = re.split(' +', line)

        if len(parts) >= 4:
            match = re.search('^goupile@([0-9A-Za-z_\\-\\.]+)\\.service$', parts[1])

            if match is not None:
                name = match.group(1)

                status = ServiceStatus()
                status.running = (parts[3] == 'active')

                if status.running:
                    try:
                        pid = int(subprocess.check_output(['systemctl', 'show', '-p', 'ExecMainPID', '--value', parts[1]]))

                        sb = os.stat(f'/proc/{pid}/exe')
                        status.inode = sb.st_ino
                    except Exception:
                        status.running = False

                services[name] = status

    return services

def run_service_command(domain, cmd):
    service = f'goupile@{domain}.service'
    print(f'  + {cmd.capitalize()} {service}', file = sys.stderr)
    execute_command(['systemctl', cmd, '--quiet', service])

def update_domain_config(domain, info, auto, smtp, sms):
    filename = os.path.join(info.main_directory, 'goupile.ini')

    ini = configparser.ConfigParser()
    ini.optionxform = str

    ini.add_section('Domain')
    ini.set('Domain', 'Title', domain)
    ini.set('Domain', 'DemoMode', 'On' if info.demo_mode else 'Off')

    ini.add_section('Data')
    ini.set('Data', 'ArchiveDirectory', info.archive_directory)
    ini.set('Data', 'SnapshotDirectory', info.snapshot_directory)

    ini.add_section('Archives')
    ini.set('Archives', 'PublicKey', info.archive_key)
    if auto is not None:
        for key, value in auto.items():
            ini.set('Archives', key, value)

    ini.add_section('HTTP')
    ini.set('HTTP', 'SocketType', 'Unix')
    ini.set('HTTP', 'UnixPath', info.socket)
    ini.set('HTTP', 'ClientAddress', 'X-Forwarded-For')

    if smtp is not None:
        ini.add_section('SMTP')
        for key, value in smtp.items():
            ini.set('SMTP', key, value)

    if sms is not None:
        ini.add_section('SMS')
        for key, value in sms.items():
            ini.set('SMS', key, value)

    with io.StringIO() as f:
        ini.write(f)
        return commit_file(filename, f.getvalue())

def run_sync(config, key_dir):
    binary = config['Goupile.BinaryFile']

    # List existing domains and services
    domains = list_domains(key_dir, config['Goupile.DomainDirectory'],
                                    config['Goupile.ArchiveDirectory'],
                                    config['Goupile.SnapshotDirectory'])
    services = list_services()

    # Create missing domains
    print('>>> Create new domains', file = sys.stderr)
    for domain, info in domains.items():
        if not os.path.exists(info.main_directory):
            create_domain(binary, domain, info.main_directory, info.archive_key,
                          config['Goupile.RunUser'], config['Goupile.RunGroup'],
                          config['Goupile.DefaultAdmin'], config['Goupile.DefaultPassword'])

    # Detect binary mismatches
    print('>>> Check Goupile binary versions', file = sys.stderr)
    for domain, info in domains.items():
        domain_binary = os.path.join(info.main_directory, 'goupile')

        try:
            inode = os.stat(domain_binary).st_ino
        except FileNotFoundError:
            os.symlink(binary, domain_binary)
            inode = os.stat(domain_binary).st_ino

        status = services.get(domain)
        if status is not None and status.running and status.inode != inode:
            print(f'  + Domain {domain} is running old version')
            info.mismatch = True

    changed = False

    # Create missing archive and snapshot directories
    for info in domains.values():
        if not os.path.exists(info.archive_directory):
            os.mkdir(info.archive_directory, mode = 0o700)
            shutil.chown(info.archive_directory, config['Goupile.RunUser'], config['Goupile.RunGroup'])

            info.mismatch = True
            changed = True

        if not os.path.exists(info.snapshot_directory):
            os.mkdir(info.snapshot_directory, mode = 0o700)
            shutil.chown(info.snapshot_directory, config['Goupile.RunUser'], config['Goupile.RunGroup'])

            info.mismatch = True
            changed = True

        os.chmod(info.snapshot_directory, 0o700)

    # Update instance configuration files
    print('>>> Update Goupile configuration files', file = sys.stderr)
    for domain, info in domains.items():
        auto = config.get('Archives')
        smtp = config.get('SMTP')
        sms = config.get('SMS')
        if update_domain_config(domain, info, auto, smtp, sms):
            info.mismatch = True
            changed = True

    # Sync systemd services
    print('>>> Sync systemd services', file = sys.stderr)
    for domain in services:
        info = domains.get(domain)
        if info is None:
            run_service_command(domain, 'stop')
            run_service_command(domain, 'disable')
            changed = True
    for domain, info in domains.items():
        status = services.get(domain)
        if status is None:
            run_service_command(domain, 'enable')
        if status is None or info.mismatch or not status.running:
            run_service_command(domain, 'restart')
            changed = True

    # Nothing changed!
    if not changed:
        print('>>> Nothing has changed', file = sys.stderr)
        return

if __name__ == '__main__':
    # Always work from sync.py directory
    script = os.path.abspath(__file__)
    directory = os.path.dirname(script)
    os.chdir(directory)

    # Parse configuration
    config = load_config('sync.ini')
    config['Goupile.BinaryFile'] = os.path.abspath(config['Goupile.BinaryFile'])
    config['Goupile.DomainDirectory'] = os.path.abspath(config['Goupile.DomainDirectory'])
    config['Goupile.ArchiveDirectory'] = os.path.abspath(config['Goupile.ArchiveDirectory'])
    config['Goupile.SnapshotDirectory'] = os.path.abspath(config['Goupile.SnapshotDirectory'])

    run_sync(config, 'domains')
