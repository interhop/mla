# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see https://www.gnu.org/licenses/.

- name: Configure Lufi user
  user:
    name: lufi
    state: present
  become: yes

- name: Install packages
  apt:
    update_cache: yes
    pkg:
      - perl
      - carton
      - build-essential
      - libssl-dev
      - libio-socket-ssl-perl
      - liblwp-protocol-https-perl
      - zlib1g-dev
      - libpq-dev
      - libsqlite3-dev
      - libmariadb-dev
      - acl
    state: latest
  become: yes

- name: Set up mysql_config symbolic link
  file:
    path: /usr/local/bin/mysql_config
    src: /usr/bin/mariadb_config
    state: link
  ignore_errors: yes
  become: yes

- name: Prepare Lufi base directories
  file:
    path: '{{ item }}'
    state: directory
    owner: root
    group: root
    mode: '0755'
  loop:
    - '{{ lufi_root_path }}'
    - '{{ lufi_root_path }}/{{ lufi_name }}'
  become: yes

- name: Prepare Lufi app directories
  file:
    path: '{{ item }}'
    state: directory
    owner: lufi
    group: lufi
    mode: '0755'
  loop:
    - '{{ lufi_root_path }}/{{ lufi_name }}/app'
    - '{{ lufi_root_path }}/{{ lufi_name }}/files'
  become: yes

- name: Fetch Lufi repository
  git:
    repo: '{{ lufi_repo }}'
    version: '{{ lufi_commit }}'
    dest: '{{ lufi_root_path }}/{{ lufi_name }}/app'
    force: yes
  become: yes
  become_user: lufi

- name: Install Lufi dependencies
  command:
    cmd: 'carton install --deployment --without=test'
    chdir: '{{ lufi_root_path }}/{{ lufi_name }}/app'
  register: carton
  changed_when: '(carton.stderr_lines | length) > 0 or (carton.stdout_lines | length) > 2'
  become: yes
  become_user: lufi

- name: Generate random salt
  block:
  - name: Generate random salt 1/4
    stat:
      path: '{{ lufi_root_path }}/{{ lufi_name }}/salt'
    register: salt1
    become: yes
  - name: Generate random salt 2/4
    copy:
      dest: '{{ lufi_root_path }}/{{ lufi_name }}/salt'
      owner: root
      group: root
      mode: '0644'
      content: '{{ lookup("community.general.random_string", length=32, special=False) }}'
    when: 'not salt1.stat.exists'
    become: yes
  - name: Generate random salt 3/4
    slurp:
      src: '{{ lufi_root_path }}/{{ lufi_name }}/salt'
    register: salt2
    become: yes
  - name: Generate random salt 4/4
    set_fact:
      lufi_salt: '{{ salt2.content | b64decode }}'

- name: Update configuration files
  template:
    src: '{{ item.src }}'
    dest: '{{ item.dest }}'
    owner: root
    group: root
    mode: '0644'
  loop:
    - src: lufi.conf.j2
      dest: '{{ lufi_root_path }}/{{ lufi_name }}/app/lufi.conf'
    - src: lufi@.service.j2
      dest: /etc/systemd/system/lufi@.service
  become: yes

- name: Restart Lufi service
  service:
    name: 'lufi@{{ lufi_name }}.service'
    state: restarted
    enabled: yes
  when: 'not ansible_check_mode'
  become: yes
