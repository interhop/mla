# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see https://www.gnu.org/licenses/.

- name: Configure Agnos user
  user:
    name: agnos
    state: present
  become: yes

- name: Create Agnos root directory
  file:
    path: '{{ agnos_root_path }}'
    state: directory
    owner: root
    group: root
    mode: '0755'
  become: yes

- name: Create Agnos data directories
  file:
    path: '{{ item }}'
    state: directory
    owner: agnos
    group: www-data
    mode: '0750'
  loop:
    - '{{ agnos_root_path }}/accounts'
    - '{{ agnos_root_path }}/certificates'
  become: yes

- name: Download Agnos binary
  get_url:
    url: '{{ agnos_url }}'
    dest: '{{ agnos_root_path }}/agnos'
    checksum: '{{ agnos_checksum }}'
    owner: root
    group: root
    mode: '0755'
  become: yes

- name: Create Agnos account keys
  shell:
    cmd: 'openssl genrsa 4096 > {{ item }}.key'
    chdir: '{{ agnos_root_path }}/accounts'
    creates: '{{ agnos_root_path }}/accounts/{{ item }}.key'
  loop: '{{ dns_le_accounts | dict2items | map(attribute = "key") }}'
  become: yes

- name: Configure Agnos for DNS-01 challenges
  template:
    src: agnos.toml.j2
    dest: '{{ agnos_root_path }}/agnos.toml'
    owner: root
    group: root
    mode: '0644'
  become: yes

- name: Install scheduled Agnos systemd units
  template:
    src: '{{ item }}.j2'
    dest: '/etc/systemd/system/{{ item }}'
    owner: root
    group: root
    mode: '0644'
  loop:
    - agnos.service
    - agnos.timer
  become: yes

- name: Reload systemd units
  systemd:
    daemon_reload: yes
  when: 'not ansible_check_mode'
  become: yes

- name: Start Agnos to update certificates
  command:
    cmd: '{{ agnos_root_path }}/agnos --no-staging {{ agnos_root_path }}/agnos.toml'
    chdir: '{{ agnos_root_path }}'
  when: 'not ansible_check_mode'
  register: agnos
  failed_when: 'agnos.rc != 0 and "error:rateLimited" not in agnos.stderr'
  become: yes
  become_user: agnos

- name: Enabled scheduled renewals
  service:
    name: agnos.timer
    state: started
    enabled: yes
  become: yes
