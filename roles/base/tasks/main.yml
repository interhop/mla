# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see https://www.gnu.org/licenses/.

- name: Install packages
  package:
    update_cache: yes
    pkg:
      - ufw
      - docker-compose
      - docker.io
      - apparmor-utils
      - rsync
      - acl
      - gpg
      - cron
      - unattended-upgrades
      - htop
      - dfc
      - atool
      - ripgrep
      - vim
      - systemd-coredump
    state: latest
  become: yes

- name: Remove packages
  package:
    pkg:
      - mosh
    state: absent
  become: yes

- name: Avoid SSH IP binding (too fragile with VPN)
  lineinfile:
    dest: /etc/ssh/sshd_config
    regexp: '^ListenAddress'
    state: absent
  become: yes

- name: Set public SSH port
  lineinfile:
    dest: /etc/ssh/sshd_config
    regexp: '^Port'
    line: 'Port {{ ssh_port }}'
    state: present
  become: yes

- name: Disable root login over SSH
  lineinfile:
    dest: /etc/ssh/sshd_config
    regexp: '^PermitRootLogin'
    line: 'PermitRootLogin no'
    state: present
  become: yes

- name: Set root password for emergency console
  user:
    name: root
    state: present
    password: '{{ root_password | string | password_hash(salt = (root_salt | string)) }}'
  when: 'root_password is defined'
  become: yes

- name: Reload SSH server to apply new settings
  service:
    name: ssh
    state: reloaded
  when: 'not ansible_check_mode'
  become: yes

- name: Detect netplan (OVH)
  stat:
    path: /etc/netplan
  register: ovh_netplan
  become: no

- name: Configure IPv6 without netplan (OVH)
  template:
    src: ovh_ipv6_v1.cfg.j2
    dest: /etc/network/interfaces.d/51-cloud-init-ipv6.cfg
    owner: root
    group: root
    mode: '0644'
  register: ovh_ipv6_v1
  when: 'ovh_ipv6_addr is defined and not ovh_netplan.stat.exists'
  become: yes

- name: Enable IPv6 without netplan (OVH)
  service:
    name: networking
    state: restarted
  when: ovh_ipv6_v1.changed
  become: yes

- name: Configure IPv6 with netplan (OVH)
  template:
    src: ovh_ipv6_v2.yaml.j2
    dest: /etc/netplan/51-cloud-init-ipv6.yaml
    owner: root
    group: root
    mode: '0644'
  register: ovh_ipv6_v2
  when: 'ovh_ipv6_addr is defined and ovh_netplan.stat.exists'
  become: yes

- name: Enable IPv6 with netplan (OVH)
  command: 'netplan apply'
  when: ovh_ipv6_v2.changed
  become: yes

- name: Adjust server time zone
  file:
    path: /etc/localtime
    src: '/usr/share/zoneinfo/{{ timezone }}'
    state: link
  become: yes

- name: Disable ufw logging
  ufw:
    logging: off
  when: 'ufw_enable'
  become: yes

- name: Reset ufw rules
  ufw:
    state: reset
  when: 'ufw_enable and not ansible_check_mode'
  become: yes

- name: Configure ufw rules
  ufw:
    rule: '{{ item.rule }}'
    to: '{{ item.to | default("any") }}'
    port: '{{ item.port }}'
    proto: '{{ item.proto | default("any") }}'
  loop: '{{ ufw_rules }}'
  when: 'ufw_enable'
  become: yes

- name: Configure ufw defaults
  ufw:
    direction: '{{ item.direction }}'
    policy: '{{ item.policy }}'
  loop:
    - direction: incoming
      policy: deny
    - direction: outgoing
      policy: allow
  when: 'ufw_enable'
  become: yes

- name: Allow connections from Docker containers to host
  ufw:
    interface_in: docker0
    policy: allow
  when: 'ufw_enable'
  become: yes

- name: Enable ufw
  ufw:
    state: enabled
  when: 'ufw_enable'
  become: yes

- name: Reload ufw
  ufw:
    state: reloaded
  when: 'ufw_enable and not ansible_check_mode'
  become: yes

- name: Restrict ptrace scope (YAMA)
  sysctl:
    name: kernel.yama.ptrace_scope
    value: '3'
    sysctl_set: yes
  become: yes

- name: Restrict default Docker address pool
  template:
    src: docker_daemon.json.j2
    dest: /etc/docker/daemon.json
    owner: root
    group: root
    mode: '0600'
  register: docker_pool
  become: yes

- name: Restart Docker service
  service:
    name: docker
    state: restarted
    enabled: yes
  when: 'docker_pool.changed'
  become: yes

- name: Drop old weekly upgrade and reboot task
  cron:
    name: weekly_upgrade
    state: absent
  become: yes

- name: Set APT update/ugprade frequency to "always"
  copy:
    dest: /etc/apt/apt.conf.d/20auto-upgrades
    owner: root
    group: root
    mode: 0644
    content: |
      APT::Periodic::Update-Package-Lists "always";
      APT::Periodic::Unattended-Upgrade "always";
  become: yes

- name: Detect unattended origin mode
  ansible.builtin.lineinfile:
    path: /etc/apt/apt.conf.d/50unattended-upgrades
    regexp: '^Unattended-Upgrade::Origins-Pattern {'
    state: absent
  check_mode: yes
  changed_when: no
  register: unattended_pattern
  become: no

- name: Configure unattended-upgrades for all packages
  lineinfile:
    dest: /etc/apt/apt.conf.d/50unattended-upgrades
    line: '{{ item.line }}'
    insertafter: '{{ item.after }}'
    state: present
  loop:
    - after: '^Unattended-Upgrade::Origins-Pattern {'
      line: '        "o=*";'
      when: yes
    - after: 'Unattended-Upgrade::Allowed-Origins {'
      line: '        "*:*";'
      when: no
  when: 'unattended_pattern.found == item.when'
  become: yes

- name: Enable and configure auto-reboot after upgrades
  lineinfile:
    dest: /etc/apt/apt.conf.d/50unattended-upgrades
    regexp: '{{ item.regexp }}' 
    line: '{{ item.line }}'
    state: present
  loop:
    - regexp: 'Unattended-Upgrade::Automatic-Reboot '
      line: 'Unattended-Upgrade::Automatic-Reboot "true";'
    - regexp: 'Unattended-Upgrade::Automatic-Reboot-Time '
      line: 'Unattended-Upgrade::Automatic-Reboot-Time "03:00";'
  become: yes

- name: Enable unattended upgrades
  shell: dpkg-reconfigure -pmedium unattended-upgrades
  when: 'not ansible_check_mode'
  become: yes

- name: Configure systemd coredump
  template:
    src: coredump.conf.j2
    dest: /etc/systemd/coredump.conf
    owner: root
    group: root
    mode: '0644'
  become: yes

- name: Adjust systemd coredump retention period
  template:
    src: coredump.clean.j2
    dest: /etc/tmpfiles.d/coredump.conf
    owner: root
    group: root
    mode: '0644'
  become: yes

- name: Delete misnamed coredump config file
  file:
    path: /etc/tmpfiles.d/coredump.cpnf
    state: absent
  become: yes

- name: Restart systemd coredump service
  service:
    name: systemd-coredump.socket
    state: restarted
  when: 'not ansible_check_mode'
  become: yes

- name: Configure CoreDNS server
  include_tasks: coredns.yml
  when: 'dns_server'

- name: Configure Agnos for DNS-01 challenges
  include_tasks: agnos.yml
  when: 'dns_server'

- name: Create destination folder for certificates
  file:
    path: '/opt/certificates'
    state: directory
    owner: root
    group: www-data
    mode: '0750'
  become: yes

- name: Create temporary folder for Agnos certificates
  file:
    path: tmp/agnos
    state: directory
  delegate_to: localhost
  run_once: yes
  when: 'not ansible_check_mode'
  become: no

- name: Fetch Agnos certificates
  synchronize:
    src: '{{ agnos_root_path }}/certificates/'
    dest: 'tmp/agnos/'
    mode: pull
    archive: no
    recursive: yes
    rsync_opts: '--mkpath'
  when: 'dns_server and not ansible_check_mode'
  become: yes

- name: Distribute Agnos certificates
  synchronize:
    src: 'tmp/agnos/'
    dest: '/opt/certificates/'
    archive: no
    recursive: yes
  when: 'not ansible_check_mode'
  become: yes

- name: Adjust certificate permissions
  file:
    path: '/opt/certificates/'
    state: directory
    owner: root
    group: www-data
    mode: 'u=rwX,g=rX,o='
    recurse: yes
  become: yes

- name: Delete local certificates
  file:
    path: tmp/agnos
    state: absent
  delegate_to: localhost
  run_once: yes
  become: no
