# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see https://www.gnu.org/licenses/.

- name: Configure napka user
  user:
    name: napka
    state: present
  become: yes

- name: Configure Node repository key
  apt_key:
    url: 'https://deb.nodesource.com/gpgkey/nodesource.gpg.key'
    id: '68576280'
    state: present
  become: yes

- name: Configure Node 16.x repository
  apt_repository:
    repo: 'deb https://deb.nodesource.com/node_16.x {{ ansible_distribution_release }} main'
    state: present
  become: yes

- name: Install packages
  apt:
    update_cache: yes
    pkg:
      - nodejs
      - unzip
      - git
      - rsync
      - acl
      - sqlite3
    state: latest
  become: yes

- name: Prepare Napka base directories
  file:
    path: '{{ item.name }}'
    state: directory
    owner: root
    group: root
    mode: '{{ item.mode }}'
  loop:
    - name: '{{ napka_root_path }}'
      mode: '0755'
    - name: '{{ napka_root_path }}/builds'
      mode: '0755'
    - name: '{{ napka_root_path }}/secrets'
      mode: '0700'
    - name: '{{ napka_root_path }}/backups'
      mode: '0700'
  become: yes

- name: Prepare Napka repository directory
  file:
    path: '{{ napka_root_path }}/repo'
    state: directory
    owner: '{{ ansible_user | default(lookup("env", "USER"), True) }}'
    recurse: yes
  become: yes

- name: Prepare Napka working directories
  file:
    path: '{{ item }}'
    state: directory
    owner: napka
    group: napka
    mode: '0755'
  loop:
    - '{{ napka_root_path }}/cache'
    - '{{ napka_root_path }}/databases'
    - '{{ napka_root_path }}/builds/{{ napka_name }}'
  become: yes

- name: Fetch Napka repository
  git:
    repo: '{{ napka_repo }}'
    version: '{{ napka_commit }}'
    dest: '{{ napka_root_path }}/repo'
    force: yes
  become: no

- name: Install Napka code
  synchronize:
    src: '{{ napka_root_path }}/repo/src/napka/'
    dest: '{{ napka_root_path }}/builds/{{ napka_name }}/src/napka'
    delete: yes
    recursive: yes
    owner: false
    group: false
    rsync_opts:
      - "--mkpath --exclude=node_modules/"
  delegate_to: '{{ inventory_hostname }}'
  become: yes
  become_user: napka

- name: Install Napka dependencies
  synchronize:
    src: '{{ napka_root_path }}/repo/src/web/'
    dest: '{{ napka_root_path }}/builds/{{ napka_name }}/src/web/'
    delete: yes
    recursive: yes
    owner: false
    group: false
    rsync_opts:
      - "--mkpath"
  delegate_to: '{{ inventory_hostname }}'
  become: yes
  become_user: napka

- name: Run NPM install
  command:
    cmd: 'npm install --no-audit'
    chdir: '{{ napka_root_path }}/builds/{{ napka_name }}/src/napka'
  register: npm_install
  changed_when: '"up to date" not in npm_install.stdout'
  become: yes
  become_user: napka

- name: Install Napka service file
  template:
    src: napka@.service.j2
    dest: '/etc/systemd/system/napka@.service'
    owner: root
    group: root
    mode: '0644'
  become: yes

- name: Install scheduled Napka import units
  template:
    src: '{{ item }}.j2'
    dest: '/etc/systemd/system/{{ item }}'
    owner: root
    group: root
    mode: '0644'
  loop:
    - napka_import@.service
    - napka_import@.timer
  become: yes

- name: Install Napka ENV file
  template:
    src: ENV.j2
    dest: '{{ napka_root_path }}/secrets/{{ napka_name }}.env'
    owner: root
    group: root
    mode: '0644'
  become: yes

- name: Create backup script
  template:
    src: backup.sh.j2
    dest: '{{ napka_root_path }}/backup.sh'
    owner: root
    group: root
    mode: '0755'
  become: yes

- name: Install scheduled backup systemd units
  template:
    src: '{{ item }}.j2'
    dest: '/etc/systemd/system/{{ item }}'
    owner: root
    group: root
    mode: '0644'
  loop:
    - napka_backup.service
    - napka_backup.timer
  become: yes

- name: Reload systemd units
  systemd:
    daemon_reload: yes
  when: 'not ansible_check_mode'
  become: yes

- name: Restart Napka services
  service:
    name: 'napka@{{ napka_name }}'
    state: restarted
    enabled: yes
  when: 'not ansible_check_mode'
  become: yes

- name: Enable scheduled imports
  service:
    name: 'napka_import@{{ napka_name }}.timer'
    state: started
    enabled: yes
  become: yes

- name: Enable scheduled backups
  service:
    name: napka_backup.timer
    state: started
    enabled: yes
  become: yes
